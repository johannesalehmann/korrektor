# Korrektor

*Die aktuellste binary befindet sich unter "Project Overiew -> [Releases](https://git.rwth-aachen.de/johannesalehmann/korrektor/-/releases)".*

Dieses Programm unterstützt bei der Korrektur von Moodle-Abgaben, indem es 

- die heruntergeladenen Abgaben nach Gruppen zusammenfasst,
- automatisch einen PDF-Editor der Wahl für die Abgabe öffnen kann,
- anschließend die Korrektur-Datei erkennt (und später für den Moodle-Upload passend in einen Ordner sortiert) und
- Punkte erfassen und automatisch in eine Moodle-Konforme csv-Datei schreibt.

Da Moodle-Räume sich leicht unterscheiden, ist die aktuelle Version vermutlich nur mit dem BuK-Raum kompatibel.

## Verwendung

Das Programm ist in C# geschrieben, benötigt also unter Linux und MacOS das mono-Framework. Unter neueren Windows-Versionen gibt es keine solchen Voraussetzungen. Es kann dann in der Kommandozeile mit

- `mono Corrector.exe pfad_zu_abgaben pfad_zu_korrekturprogramm [-l]` unter Linux und MacOS und
- `Corrector.exe pfad_zu_abgaben [pfad_zu_korrekturprogramm]` unter Windows gestartet werden.

Hier bei ist

- `pfad_zu_abgaben` der Pfad, in dem die aus Moodle heruntergeladenen Abgaben liegen. Die ZIP-Datei muss bereits extrahiert worden sein und `pfad_zu_abgaben` so gewählt sein, dass die Abgaben über `pfad_zu_abgaben/Abgabe123-Mustermann, Max [...]` erreichbar sind.
- `pfad_zu_korrekturprogramm` ist der Befehl, mit dem das Korrekturprogramm gestartet wird. Dies könnte also z. B. `"C:\Program Files\Xournal++\bin\xournalpp.exe"` unter Windows sein oder `/bin/xournalpp` unter Linux. Das Programm ist nur mit Xournal++ getestet, kann aber prinzipiell jedes Korrekturprogramm öffnen. Dieses kriegt als einziges Kommandozeilenargument den Pfad zum PDF übergeben.
- `-l` ist eine Flag, die unter Linux gesetzt werden kann, falls die Linux-Erkennung nicht automatisch funktioniert. Dies sollte jedoch nur unter Wine nötig sein. Vielleicht ist dies auch bei MacOS hilfreich.

Die Reihenfolge der Argumente muss der obigen entsprechen, da mein Parser dafür etwas primitiv ist :)

## Programmstart

Hier sollte man kurz überprüfen, dass das Betriebssystem richtig erkannt wurde (also `Windows detected!` oder `Linux detected!` erscheint -- MacOS kann ich nicht testen, aber es sollte sich wie Linux verhalten).

Anschließend kann man den Gruppenbereich wählen, in dem man korrigieren möchte. Diese Zahlen sind jeweils inklusive. Es ist unproblematisch, wenn eine Gruppe mit genau der gewählten Zahl nicht existiert (man kann also durchaus Minimum 0 und Maximum 123456 wählen, wenn man alles alleine korrigieren will :) ).

Man sollte den Bereich jedoch so wählen, dass am Ende alle Abgaben im Bereich korrigiert sind -- anderes führt möglicherweise zu Problemen beim Export.

## Generelle Hinweise

Das Programm speichert Punkte und andere Metadaten automatisch sofort zwischen (dafür wird in `pfad_zu_abgaben` für jede Gruppe eine Datei erstellt), es muss also nicht während des gesamten Korrekturvorgangs geöffnet sein.

## Der Übersichtsbildschirm

![Übersichtsbildschirm](/overview.png)

Hier kann man durch die Gruppen navigieren und kriegt eine kurze Übersicht angezeigt. Für das Flackern möchte ich mich schon mal im Voraus entschuldigen; die Windows-Konsole ist ziemlich langsam.

Die Export-Optionen werden weiter unten erklärt.

## Der Gruppenbildschirm

Mit Enter öffnet man den Gruppenbildschirm. Die dort angezeigten Nummern sind leider nicht die Matrikelnummern, sondern Moodle-interne Identifier. Es werden alle abgegebenen Datein angezeigt. Falls es mehrere gibt, kann man mit `s` die richtige wählen. Dies sollte aber nicht notwendig sein, da Moodle nur einen PDf-Upload zulässt.

### Eine Aufgabe korrigieren

`o` öffnet den zu Beginn spezifizierten PDF-Editor. Während dieser offen ist, kann Korrektor nicht benutzt werden. Dateinamen mit Sonderzeichen führen hier möglicherweise zu Problemen. In diesem Fall hilft es nur, die Datei manuell umzubenennen.

In dem Korrekturprogramm kann man dann die Korrektur vornehmen. Anschließend speichert man die Korrektur als pdf unter beliebigem Namen im gleichen Ordner. Man sollte lediglich die ursprüngliche Datei *nicht* überschreiben. Falls es eine Projektdatei gibt (bei xournal++ zum Beispiel `.xopp`), kann man diese ebenfalls speichern.

Beendet man nun das Programm, sollte sowohl die PDf als die Projektdatei erkannt werden.

![Identifizierte Projekt- und Korrekturdatei](/project-file.png)

Möchte man noch etwas an der Korrektur ändern, so wird in Zukunft automatisch die Projektdatei geöffnet, sofern eine existiert.

### Punkte eintragen

Mit `p` geht man in den Punkte-Eintrag-Modus. Hier sollte man die Gesamtpunktzahl eintragen, *nicht die Punktzahlen der einzelnen Aufgaben*. Dies wird zwar unterstützt, ist aber in der BuK-Vorlesung nicht notwendig. Dafür trägt man die Punkte bei A1 ein, bestätigt mit Enter und beendet die Punkteeingabe durch ein zweites *Enter*. (Leider ist es nicht ganz trivial, hier auf Escape zu hören, daher wird das nicht unterstützt).

### Zur nächsten Gruppe

Man kann mit Pfeiltasten von einem Gruppenbildschirm direkt zum nächsten oder mit `Escape` zurück zur Übersicht.

## Datein für den Upload exportieren

Ist man mit der Korrektur fertig, muss man zum einen die korrigierten PDFs zu Moodle hochladen, zum anderen die Punkte (dies sind zwei separate Schritte).

### Korrigierte PDFs

Diese werden exportiert, indem man `e` im Übersichtsbildschirm betätigt. Hierbei werden einheitliche Namen gegeben -- die selbstgewählten Namen der Korrektur-PDFs bleiben also nicht erhalten. Der Ordner enhält eine Kopie des PDFs für jedes Gruppenmitglied, da dies ansonsten mitunter dazu führte, dass nicht alle Mitglieder die Gruppe sehen konnten.

Falls Korrekturdateien fehlen, wird eine Warnung ausgegeben. Der *Inhalt* des exportierten Ordners muss danach gezipt werden (es müssen also im Zip-Ordner direkt die einzelnen Ordner für die Gruppen liegen, nicht ein Unterordner `korrekturen`). Dieser kann bei Moodle hochgeladen.

## Punkte

Für den Punkte-Export muss zunächst die CSV-Datei *für das passende Übungsblatt* aus Moodle heruntergeladen werden:

![Bewertungsworkflow, dort Bewertungstabelle herunterladen wählen](/bewertungsvorgang.png)

Diese speichert man nun unter `pfad_zu_abgaben`. Vom Übersichtsbildschirm aus kann man mit `p` den Export starten. Falls mehrere csv-Datein gefunden werden, kann man eine wählen.

Anschließend werden möglicherweise Warnungen ausgegeben. Diese bitte genau lesen, sie deuten darauf hin, dass etwas beim Parsen der csv-Datei schiefgegangen ist. Ansonsten muss man den Export noch einmal mit `Y` bestätigen.

Die Datei kann nun im Moodle hochgeladen werden. Hierbei muss die Option *Update von Datensätzen zulassen, die seit dem letzten Upload angepasst wurden.* aktiviert werden. Falls jemand versteht, was an meiner CSV-Datei falsch ist, möge man mir dies bitte mitteilen :)

![Screenshot des Punkte-Hochlade-Interfaces](/upload.png)

Anschließend sollte man eine lange Liste bekommen, welche Personen welche Punktzahlen zugeordnet bekommen. Auch hier ist es sinnvoll, noch mal kurz zu überprüfen, ob alles mit rechten Dingen zugeht, da man hier sonst schnell vorhandene Punkte überschreibt. Falls in diesem Schritt etwas schiefgeht, kann ich dafür leider keine Haftung übernehmen :)