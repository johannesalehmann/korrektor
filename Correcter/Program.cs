﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Correcter
{
    public static class Program
    {
        private static Executor cmd;

        private static string pdfAnnotator;
        private static string path;

        private static bool linux;
        private static bool debug;

        private static int min, max;
        
        public static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Bitte mindestens mit einem Parameter aufrufen.");
                Console.WriteLine("Die Hilfe ist mit -h verfügbar.");
                return;
            }

            if (args[0] == "h" || args[0] == "-h" || args[0] == "help" || args[0] == "-help")
            {
                showHelp();
                return;
            }

            // Not super pretty, but I don't want to switch to .NET core
            int p = (int)Environment.OSVersion.Platform;
            linux = (p == 4) || (p == 6) || (p == 128);

            if (args.Any(arg => arg == "-l" || arg == "-linux"))
            {
                linux = true;
                message("Linux manuell gewählt.", MsgType.Info);
            }
            if (args.Any(arg => arg == "-d" || arg == "-debug"))
            {
                debug = true;
                message("Debug-Ausgaben aktiviert.", MsgType.Info);
            }

            path = args[0];
            if (!path.EndsWith(Path.DirectorySeparatorChar.ToString()))
                path += Path.DirectorySeparatorChar;
            pdfAnnotator = "C:\\Program Files\\Xournal++\\bin\\xournalpp.exe";
            if (args.Length >= 2)
                pdfAnnotator = args[1];
            else
                message("Kein PDF-Annotator angegeben. Der Default ist \"" + pdfAnnotator + 
                                  "\". Möglicherweise ist dieser nicht installiert!", linux ? MsgType.Error : MsgType.Warning);

            if (args.Length > 2)
                message("Nur die ersten beiden Argumente werden benutzt.", MsgType.Warning);


            if (linux)
                message("Linux detected!", MsgType.Info);
            else
                message("Windows detected!", MsgType.Info);

            if (!linux)
                pdfAnnotator = "\"" + pdfAnnotator + "\"";
            cmd = new Executor(path, linux);
            
            message("Erfolgreich initialisiert.", MsgType.Info);
            message("\nZu korrigierenden Bereich wählen:");
            min = InputHelper.getInt("Bitte die erste Gruppennummer eingeben:");
            max = InputHelper.getInt("Bitte die letzte Gruppennummer eingeben:");

            var groups = getGroups(path);

            drawGroupList(groups);
        }

        private static void showHelp()
        {
            Console.WriteLine(@"  _  __                   _    _             
 | |/ /                  | |  | |            
 | ' / ___  _ __ _ __ ___| | _| |_ ___  _ __ 
 |  < / _ \| '__| '__/ _ \ |/ / __/ _ \| '__|
 | . \ (_) | |  | | |  __/   <| || (_) | |   
 |_|\_\___/|_|  |_|  \___|_|\_\\__\___/|_|   
                                             ");

            Console.WriteLine("Dieses Programm unterstützt bei der Korrektur von Abgaben, die per Moodle gemacht wurden.");
            Console.WriteLine("\nVerwendung unter Windows:\n    Corrector.exe pfad_zu_abgaben [pfad_zu_korrekturprogramm]");
            Console.WriteLine("\nVerwendung unter Linux (und MacOS?):\n    mono Corrector.exe pfad_zu_abgaben [pfad_zu_korrekturprogramm] [-l]");
            Console.WriteLine("\n    pfad_zu_abgaben zeigt auf das Verzeichnis, in dem die von Moodle exportierten Abgaben liegen. Das Verzeichnis sollte also für jeden Studierenden einen Ordner enthalten, in dem wiederum eine PDF-Datei liegt.");
            Console.WriteLine("\n    pfad_zu_korrekturprogramm ist der Befehl, mit dem das PDF-Annotationsprogramm geöffnet werden kann. Standardmäßig ist dies \"C:\\Program Files\\Xournal++\\bin\\xournalpp.exe\", was unter Windows möglicherweise funktioniert, unter Linux vermutlich eher nicht. Das Korrekturprogramm wird von Korrektur dann mit einem Parameter aufgerufen, der auf die zu korrigierenden PDF-Datei zeigt. Es sollte also prinzipiell möglich sein, ein beliebiges Programm zu verwenden -- getestet ist allerdings nur Xournal++.");
            Console.WriteLine("\n    -l überschreibt die Betriebssystemerkennung und markiert, dass das Programm unter Linux gestartet wurde.");
        }

        private static void drawGroupList(List<Group> groups)
        {
            if (groups.Count == 0)
            {
                Console.WriteLine("Es wurden keine Gruppen gefunden.");
                return;
            }
            
            int selected = 0;
            while (true)
            {
                Console.Clear();
                
                for (int i = 0; i < groups.Count; i++)
                {
                    if (i == selected)
                        Console.Write("-> ");
                    else
                        Console.Write("   ");
                    Group group = groups[i];
                    Console.Write(("Gruppe " + group.number).PadLeft("Gruppe 100".Length));
                    Console.Write(": ");
                    if (group.correctedPdf == null)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(" nicht korrigiert ");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    else
                    {
                        Console.Write("    korrigiert    ");
                    }

                    for (var pIndex = 0; pIndex < group.points.Count; pIndex++)
                    {
                        var point = group.points[pIndex];
                        Console.Write("  A" + pIndex + ":" + point.ToString("0.0").PadLeft("100.0".Length));
                    }

                    Console.WriteLine();
                }

                Console.WriteLine(String.Concat(Enumerable.Repeat("-", Console.BufferWidth)));
                Console.WriteLine("[+][-][Pos1][Ende]: Gruppe wählen  (Shift springt 5 Gruppen auf einmal)");
                Console.WriteLine("[Enter]:            Aktuelle Gruppe öffnen");
                Console.WriteLine("[e]:                Alle Korrekturen exportieren (Shift für detaillierten Output)");
                Console.WriteLine("[p]:                Punkte exportieren (hierfür muss die Punkte-csv-Datei aus dem Moodle im gleichen Verzeichnis liegen)");
                

                ConsoleKeyInfo command = Console.ReadKey(true);
                if (command.Key == ConsoleKey.End)
                {
                    selected = groups.Count - 1;
                }
                else if (command.Key == ConsoleKey.Home)
                {
                    selected = 0;
                }
                int delta = 0;
                if (command.Key == ConsoleKey.UpArrow || command.Key == ConsoleKey.OemMinus)
                    delta = -1;
                if (command.Key == ConsoleKey.DownArrow || command.Key == ConsoleKey.OemPlus)
                    delta = 1;
                if (command.Modifiers != 0)
                    delta *= 5;

                
                selected += delta;
                selected = clamp(selected, 0, groups.Count - 1);

                if (command.Key == ConsoleKey.Enter)
                {
                    int result;
                    do
                    {
                        result = processGroup(groups[selected]);
                        selected += result;
                        selected = clamp(selected, 0, groups.Count - 1);
                    } while (result != 0);
                }

                if (command.Key == ConsoleKey.E)
                {
                    bool verbose = (command.Modifiers & ConsoleModifiers.Shift) != 0; // Doesn't actually do anything yet :(
                    
                    int index = 1;
                    string directoryName = "korrekturen";
                    while (Directory.Exists(path + directoryName))
                    {
                        index++;
                        directoryName = "korrekturen" + index;
                    }

                    string dirPath = path + directoryName + Path.DirectorySeparatorChar;
                    Directory.CreateDirectory(dirPath);
                    foreach (var dir in Directory.GetDirectories(path))
                    {
                        string local = dir.Substring(path.Length);
                        if (!parseGroupName(local, out int groupIndex))
                            continue;
                        Group group = groups.FirstOrDefault(gr => gr.number == groupIndex); // At this point, the datastructure is starting to feel slightly inadequate
                        if (group != null)
                        {
                            if (group.correctedPdf == null)
                            {
                                Console.WriteLine("Keine Korrektur für Gruppe " + group.number + " gefunden.");
                            }
                            else
                            {
                                string name = "korrektur-gruppe-" + group.number + ".pdf";
                                Directory.CreateDirectory(dirPath + local);
                                File.Copy(group.correctedPdf,
                                    dirPath + local + Path.DirectorySeparatorChar + name);
                            }
                            
                        }
                    }
                    
                    Console.WriteLine("Die Korrekturen befinden sich im Verzeichnis \"" + directoryName + "\"");
                    
                    Console.WriteLine("Beliebige Taste zum Fortfahren drücken");
                    Console.ReadKey();
                }

                if (command.Key == ConsoleKey.P)
                {
                    var files = Directory.GetFiles(path, "*.csv");
                    if (files.Length == 0)
                    {
                        Console.WriteLine("Es wurde keine .csv-Datei gefunden (diese muss in \"" + path + "\" liegen).");
                    }
                    else
                    {
                        string file = null;
                        if (files.Length == 1)
                        {
                            file = files[0];
                        }
                        else
                        {
                            Console.WriteLine("Mehrere Datein gefunden:");
                            for (int i = 0; i < files.Length; i++)
                            {
                                Console.WriteLine(" " + (i + 1).ToString().PadLeft(2) + ": " + files[i]);
                            }

                            Console.Write("Datei-Index wählen: ");
                            if (int.TryParse(Console.ReadLine(), out int fileIndex))
                            {
                                if (fileIndex < 1)
                                    Console.WriteLine("Der Datei-Index muss positiv sein");
                                else if (fileIndex > files.Length)
                                    Console.WriteLine("Der Datei-Index ist zu groß");
                                else
                                    file = files[fileIndex - 1];
                            }
                        }

                        if (file != null)
                        {
                            exportPoints(file, groups);
                        }
                    }

                    Console.WriteLine("Beliebige Taste zum Fortfahren drücken");
                    Console.ReadKey();
                }
            }
        }

        struct Member
        {
            public string id, name, matrikelnummer, status, gruppe, bewertung, bestwertung, kannAendern, zuletztGeaendertAbgabe, zuletztGeaendertBewertung, feedback;
            private bool[] wasQuoted;
            
            public Member(string row)
            {
                if (row.Contains("$")) // Let's hope that this is very unlikely to occur
                {
                    Console.WriteLine("Die csv-Datei darf das $-Zeichen nicht enhalten (das ist leider etwas ungünstig gelöst -- Enschuldigung!)");
                    //TODO: Throw exception here (and handle it whereever this constructor is called...)
                }
                row = markUnquotedCommas(row, "$");
                string[] split = row.Split('$');
                if (split.Length != 11)
                {
                    throw new ArgumentException("Die csv-Datei enthält nicht die richtige Anzahl an Spalten");
                }

                wasQuoted = new bool[11];
                for (int i = 0; i < 11; i++)
                {
                    string trimmed = split[i].Trim('\"');
                    if (split[i].Length != trimmed.Length)
                        wasQuoted[i] = true;
                    split[i] = trimmed;
                }
                id = split[0];
                name = split[1];
                matrikelnummer = split[2];
                status = split[3];
                gruppe = split[4];
                bewertung = split[5];
                bestwertung = split[6];
                kannAendern = split[7];
                zuletztGeaendertAbgabe = split[8];
                zuletztGeaendertBewertung = split[9];
                feedback = split[10];
            }

            private static string markUnquotedCommas(string original, string marker)
            {
                bool quotedSection = false;
                string result = original;
                for (int i = 0; i < original.Length; i++)
                {
                    if (original[i] == '\"')
                        quotedSection = !quotedSection;
                    if (original[i] == ',' && !quotedSection)
                        result = result.Substring(0, i) + marker
                            + (i + 1 < result.Length ? result.Substring(i + 1) : "");
                }
                return result;
            }

            public string getLine()
            {
                string sep = ",";
                return quote(id, 0) + sep
                    + quote(name, 1) + sep
                    + quote(matrikelnummer, 2)
                    + sep + quote(status, 3)
                    + sep + quote(gruppe, 4)
                    + sep + quote(bewertung, 5)
                    + sep + quote(bestwertung, 6)
                    + sep + quote(kannAendern, 7)
                    + sep + quote(zuletztGeaendertAbgabe, 8, true)
                    + sep + quote(zuletztGeaendertBewertung, 9, true)
                    + sep + quote(feedback, 10);
            }

            private string quote(string str, int index, bool mandatory = false)
            {
                if (mandatory || wasQuoted[index])
                    return "\"" + str + "\"";
                return str;
            }
        }
        
        private static void exportPoints(string baseFile, List<Group> groups)
        {
            //Console.Write("Sollen nur bepunktete Mitglieder ausgegeben werden? [Y][n]: ");
            bool onlyChanged = true; // Console.ReadLine().ToLower() == "y";
            
            string[] directories = Directory.GetDirectories(path);
            
            string[] lines = File.ReadAllLines(baseFile);
            
            List<string> changedLines = new List<string>();
            List<string> unchangedLines = new List<string>();
            
            // Create a dict to keep track how many group members we have given points
            // This should always match the number of people in that group, otherwise, someone
            // has not received their points
            Dictionary<int, int> perGroupCounter = new Dictionary<int, int>();
            Dictionary<int, List<string>> perGroupMembersFound = new Dictionary<int, List<string>>();
            foreach (var group in groups)
            {
                perGroupCounter.Add(group.number, 0);
                perGroupMembersFound.Add(group.number, new List<string>());
            }
            
            for (int i = 1; i < lines.Length; i++)
            {
                Member member = new Member(lines[i]);
                bool changed = false;

                if (!tryIdentifyGroup(member, directories, out int groupNumber))
                {
                    // Console.WriteLine("Konnte \"" + member.name + "\" keiner Gruppe zuordnen (z. B. weil die Person nicht abgegeben hat)");
                }
                else
                {
                    if (groupNumber >= min && groupNumber <= max)
                    {
                        Group group = groups.FirstOrDefault(gr => gr.number == groupNumber);
                        if (group == null)
                        {
                            Console.WriteLine("Konnte Gruppe " + groupNumber +
                                              " nicht finden (das sollte eigentlich nicht passieren können)");
                        }
                        else
                        {
                            member.bewertung = group.totalPoints.ToString(CultureInfo.InvariantCulture);
                            changed = true;
                            member.zuletztGeaendertBewertung = DateTime.Now.ToLongDateString() + ", " + DateTime.Now.ToLongTimeString();

                            perGroupCounter[groupNumber]++;
                            perGroupMembersFound[groupNumber].Add(member.name);
                        }
                    }
                }

                if (changed)
                    changedLines.Add(member.getLine());
                else
                    unchangedLines.Add(member.getLine());

            }

            foreach (var group in groups)
            {
                if (group.members.Count != perGroupCounter[group.number])
                {
                    message("Unstimmigkeit festgestellt: Gruppe " + group.number + " enthält " +
                                      group.members.Count + " Mitglieder, aber es wurden " +
                                      perGroupCounter[group.number] + " Personen Punkte dieser Gruppe zugeteilt.\n Members:\n  " +
                                      string.Join("\n  ", group.members) + "\nFound members:\n  " + string.Join("\n  ", perGroupMembersFound[group.number]),
                                      MsgType.Error);
                }
            }

            List<string> res = new List<string>();
            
            res.Add(lines[0]); // Header
            res.AddRange(changedLines);
            if (!onlyChanged)
                res.AddRange(unchangedLines);
            
            if (1 + changedLines.Count + unchangedLines.Count != lines.Length)
                message("Etwas ist schiefgelaufen: Die neue Datei hat nicht die gleiche Anzahl an Zeilen wie der Input :(", MsgType.Error);
            
            Console.Write("Die Datei kann jetzt gespeichert werden. Fortfahren? [Y],[n]: ");
            string poll = Console.ReadLine();
            if (poll.ToLower() == "y")
            {
                File.WriteAllLines(baseFile.Substring(0, baseFile.LastIndexOfAny(new char[] { '\\', '/' }) + 1) + "punkte.csv", res.ToArray());
                Console.WriteLine("Datei wurde erfolgreich gespeichert");
            }
        }

        private static bool tryIdentifyGroup(Member member, string[] groupDirectories, out int group)
        {
            string modifiedName = member.name.Replace("'", "");
            string search = "-" + modifiedName + "_";
            string groupString = groupDirectories.FirstOrDefault(dir => dir.Contains(search)); // This is probably not technically guaranteed to be correct, but let's just hope no name contains _ and the ones with - aren't too common...
            if (groupString == null)
            {
                group = -1;
                return false;
            }

            if (!parseGroupName(groupString.Substring(path.Length), out int groupIndex))
            {
                group = -1;
                return false;
            }

            group = groupIndex;
            return true;
        }

        private static int clamp(int value, int min, int max)
        {
            if (value < min)
                return min;
            if (value > max)
                return max;
            return value;
        }

        private static int processGroup(Group group)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine();
                Console.WriteLine("  Gruppe " + group.number);
                Console.WriteLine();
                Console.WriteLine(String.Concat(Enumerable.Repeat("=", Console.BufferWidth)));
                Console.WriteLine();

                Console.WriteLine("Mitglieder:");
                foreach (var member in group.members)
                    Console.WriteLine("  " + member);
                if (group.members.Count == 0)
                    message("  keine Mitglieder (das sollte nie passieren)", MsgType.Error);

                Console.WriteLine();
                Console.WriteLine("Datein:");
                bool selectedFileExists = false;
                bool enableConvertCommand = false;
                
                if (group.directory == null)
                {
                    message("  der Gruppe ist kein Verzeichnis zugeordnet", MsgType.Error);
                }
                else
                {
                    string[] files = Directory.GetFiles(group.directory);
                    if (files.Length == 0)
                        Console.WriteLine("  keine Datein gefunden");
                    foreach (var file in files)
                    {
                        if (file == group.file)
                        {
                            selectedFileExists = true;
                            Console.Write("-> ");
                        }
                        else
                            Console.Write("   ");

                        Console.Write(file.Substring(file.LastIndexOf(Path.DirectorySeparatorChar) + 1));

                        Console.ForegroundColor = ConsoleColor.Cyan;
                        if (file == group.correctedPdf)
                            Console.Write(" [Korrigiertes PDF]");
                        if (file == group.correctedProject)
                            Console.Write(" [Korrektur-Projektdatei]");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine();
                    }

                    if (selectedFileExists)
                    {
                        Console.WriteLine(" [o]: Markierte Datei öffnen");
                        Console.WriteLine(" [r]: Datei in \"exercise.pdf\" umbenennen");
                    }
                    else
                    {
                        if (files.Length > 1)
                        {
                            Console.WriteLine(" [c]: Datein in eine PDF vereinigen (benötigt ImageMagick)");
                            enableConvertCommand = true;
                        }
                        if (files.Length == 1 && !files[0].ToLower().EndsWith(".pdf"))
                        {
                            Console.WriteLine(" [c]: Datei in PDF konvertieren (benötigt ImageMagick)");
                            enableConvertCommand = true;
                        }
                    }

                    if (group.directory != null)
                    {
                        Console.WriteLine(" [s]: Dateiauswahl ändern");
                    }
                }


                Console.WriteLine();

                Console.WriteLine("Punkte:");
                if (group.points.Count == 0)
                    Console.WriteLine("  keine Punkte eingetragen");
                for (int i = 0; i < group.points.Count; i++)
                {
                    Console.WriteLine("  A" + (i + 1) + ": " + group.points[i]);
                }

                string pointsVerb = group.points.Count == 0 ? "eintragen" : "überschreiben";
                Console.WriteLine(" [p]: Punkte " + pointsVerb + " (Shift zum Korrigieren, Strg zum Anhängen");
                

                var action = Console.ReadKey(true);
                if (selectedFileExists)
                {
                    if (action.Key == ConsoleKey.O)
                    {
                        string[] files = Directory.GetFiles(group.directory);

                        string path = (group.correctedProject ?? group.correctedPdf ?? group.file);
                        if (linux)
                            path = path.Replace('\\', '/');
                        cmd.execute(pdfAnnotator, " \"" + path + "\"");
                        string[] newFiles = Directory.GetFiles(group.directory);
                        message("Datein: " + newFiles.Length, MsgType.Debug);

                        newFiles = newFiles.Where(x => !x.EndsWith(".xopp~")).ToArray();
                        message("Datein (ohne .xopp~): " + newFiles.Length, MsgType.Debug);

                        if (newFiles.Length - files.Length > 2)
                        {
                            Console.WriteLine(
                                "Zu viele neue Datein hinzugefügt -- konnte korrigerte Datein nicht identifizieren");
                            Console.WriteLine("Beliebige Taste drücke, um fortzufahren");
                            Console.ReadKey(true);
                        }
                        else
                        {
                            message("Suche nach neuen Datein...", MsgType.Debug);
                            foreach (var newFile in newFiles)
                            {
                                message("    Datei: " + newFile, MsgType.Debug);
                                if (!files.Contains(newFile))
                                {
                                    message("      neu hinzugekommen!", MsgType.Debug);
                                    if (newFile.EndsWith(".pdf"))
                                    {
                                        message("      wurde als korrigiertes PDF erkannt!", MsgType.Debug);
                                        group.correctedPdf = newFile;
                                    }
                                    else
                                    {
                                        message("      wurde als Projektdatei erkannt!", MsgType.Debug);
                                        group.correctedProject = newFile;
                                    }
                                }
                            }
                        }

                        if (debug)
                        {
                            Console.WriteLine("Beliebige Taste drücke, um fortzufahren");
                            Console.ReadKey(true);
                        }

                    }
                    if (action.Key == ConsoleKey.R)
                    {
                        string newFile = group.directory + "\\exercise.pdf";
                        File.Move(group.file, newFile);
                        group.file = newFile;
                    }
                }

                if (group.directory != null)
                {
                    if (action.Key == ConsoleKey.S)
                    {
                        Console.Write("Index der Datei (1-basiert) eingeben: ");
                        string fileIndexString = Console.ReadLine();
                        if (int.TryParse(fileIndexString, out int fileIndex))
                        {
                            fileIndex--;
                            string[] files = Directory.GetFiles(group.directory);
                            if (fileIndex >= 0 && fileIndex < files.Length)
                                group.file = files[fileIndex];
                        }
                    }
                }

                if (action.Key == ConsoleKey.C)
                {
                    if (enableConvertCommand)
                    {
                        string name = "converted.pdf";
                        while (File.Exists(group.directory + Path.DirectorySeparatorChar + name))
                            name = "_" + name;
                        string command = "";
                        command += "cd \"" + group.directory + "\" & ";

                        var files = Directory.GetFiles(group.directory);
                        bool pdf = false, image = false;
                        foreach (var file in files)
                            if (file.ToLower().EndsWith(".pdf"))
                                pdf = true;
                            else
                                image = true;

                        if (image && pdf)
                        {
                            Console.WriteLine("Es ist eventuell nicht unterstützt, Bild- und PDF-Datein zu vereinigen.");
                            Console.WriteLine("Beliebige Taste drücken, um fortzufahren.");
                            Console.ReadKey();
                        }

                        if (image)
                            command += "convert * " + name;
                        else
                            command += "pdftk * cat output " + name;
                        cmd.execute(command, "");

                        string filename = group.directory + Path.DirectorySeparatorChar + name;
                        // Make sure nothing went wrong (because I think my program might crash if I point it
                        // to a non-existing file
                        if (File.Exists(filename))
                        {
                            group.file = filename;
                        }
                    }
                }

                if (action.Key == ConsoleKey.P)
                {
                    if ((action.Modifiers & ConsoleModifiers.Shift) != 0)
                    {
                        Console.Write("Aufgaben-Index: ");
                        string indexString = Console.ReadLine();
                        if (int.TryParse(indexString, out int index))
                        {
                            if (index < 1 || index > group.points.Count)
                            {
                                Console.WriteLine("Ungültiger Index. Beliebige Taste zum Fortfahren drücken");
                                Console.ReadKey();
                            }
                            else
                            {

                                Console.Write("A" + index + ": ");
                                string pointsString = Console.ReadLine();
                                if (decimal.TryParse(pointsString, out decimal points))
                                {
                                    group.points[index - 1] = points;
                                }
                                else
                                {
                                    Console.WriteLine("Ungültige Punktzahl. Beliebige Taste zum Fortfahren drücken");
                                    Console.ReadKey();
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("Ungültiger Index. Beliebige Taste zum Fortfahren drücken");
                            Console.ReadKey();
                        }
                    }
                    else
                    {
                        if ((action.Modifiers & ConsoleModifiers.Control) == 0)
                            group.points.Clear();

                        Console.WriteLine("Die Eingabe wird beendet, wenn eine Nicht-Zahl gelesen wird.");
                        
                        int index = group.points.Count + 1;
                        while (true)
                        {
                            Console.Write("A" + index + ": ");
                            
                            string pointString = Console.ReadLine();
                            if (decimal.TryParse(pointString, out decimal points))
                            {
                                group.points.Add(points);
                                index++;
                            }
                            else
                                break;

                        }
                    }
                }

                if (action.Key == ConsoleKey.L)
                {
                    bool msg = false;

                    string[] correction = Directory.GetFiles(group.directory).Where(file => file.EndsWith("-.pdf")).OrderByDescending(file => file.Length).ToArray();
                    if (correction.Length > 0)
                    {
                        group.correctedPdf = correction[0];
                    }
                    else
                    {
                        Console.WriteLine("Kein Korrektur-PDF erkannt.");
                        msg = true;
                    }

                    string[] projectFile = Directory.GetFiles(group.directory).Where(file => file.EndsWith(".xopp")).OrderByDescending(file => file.Length).ToArray();
                    if (projectFile.Length > 0)
                    {
                        group.correctedProject = projectFile[0];
                    }
                    else
                    {
                        Console.WriteLine("Keine Korrektur-Projektdatei erkannt.");
                        msg = true;
                    }
                    if (msg)
                    {
                        Console.WriteLine("Beliebige Taste zum Fortfahren drücken");
                        Console.ReadKey();
                    }
                }

                
                if (action.Key == ConsoleKey.LeftArrow)
                    return -1;
                if (action.Key == ConsoleKey.RightArrow)
                    return 1;
                if (action.Key == ConsoleKey.Escape)
                    return 0;
                
                File.WriteAllText(path + "group" + group.number + ".info", group.serialize());
            }

        }

        private static void unite()
        {
            
        }

        private static List<Group> getGroups(string basePath)
        {
            string[] dirs = Directory.GetDirectories(basePath);
            Dictionary<int, Group> map = new Dictionary<int, Group>();

            for (int i = min; i <= max; i++)
            {
                map.Add(i, new Group(i));
            }

            bool pauseBeforeReturn = false;

            foreach (var dir in dirs)
            {
                string localName = dir.Substring(basePath.Length);
                if (!parseGroupName(localName, out var groupIndex)) continue;

                if (groupIndex < min || groupIndex > max)
                    continue;

                int matrStart = localName.IndexOf("_", StringComparison.Ordinal) + 1;
                int matrEnd = localName.IndexOf("_assign", StringComparison.Ordinal);
                string matriculation = localName.Substring(matrStart, matrEnd - matrStart);

                //if (matriculation.Length != 6 || !int.TryParse(matriculation, out _))
                //{
                //    message("Ungewöhnliche Matrikelnummer (\"" + matriculation + "\") gefunden", MsgType.Warning);
                //    pauseBeforeReturn = true;
                //}

                map[groupIndex].members.Add(matriculation);
                map[groupIndex].directory = dir;
                var files = Directory.GetFiles(map[groupIndex].directory);
                if (files.Length == 1 && files[0].ToLower().EndsWith(".pdf"))
                    map[groupIndex].file = files[0];
            }

            foreach (var file in Directory.GetFiles(basePath, "group*.info"))
            {
                int startIndex = basePath.Length + "group".Length;
                int endIndex = file.Length - ".info".Length;
                string groupIndexString = file.Substring(startIndex, endIndex - startIndex);
                if (!int.TryParse(groupIndexString, out int groupIndex))
                {
                    Console.WriteLine("Konnte Gruppennummer nicht aus group*.info-Name lesen)");
                    pauseBeforeReturn = true;
                }
                else
                {
                    Group group = Group.deserialize(File.ReadAllLines(file));
                    if (group.number != groupIndex)
                    {
                        Console.WriteLine("Der Index in der group-info-Datei ist inkonsistent");
                        pauseBeforeReturn = true;
                    }
                    if (min <= group.number && group.number <= max)
                    {
                        map[group.number] = group;
                    }
                    else
                    {
                        Console.WriteLine(
                            "Group-Info für Gruppe gefunden, die nicht im aktuellen Intervall liegt (Index " +
                            group.number + ")");
                        pauseBeforeReturn = true;
                    }
                }
            }
            
            List<Group> groups = new List<Group>();
            foreach (var kvp in map)
            {
                if (kvp.Value.members.Count > 0)
                    groups.Add(kvp.Value);
            }
            
            if (pauseBeforeReturn)
            {
                Console.WriteLine("Beliebige Taste drücken, um fortzufahren.");
                Console.ReadKey();
            }

            return groups;
        }

        private static bool parseGroupName(string localName, out int groupIndex)
        {
            if (!localName.StartsWith("Abgabe") || !localName.EndsWith("_assignsubmission_file_"))
            {
                groupIndex = -1;
                return false;
            }

            int indexStart = "Abgabe".Length;
            int indexEnd = localName.IndexOf("-", StringComparison.Ordinal);
            string indexStr = localName.Substring(indexStart, indexEnd - indexStart);
            if (!int.TryParse(indexStr, out groupIndex))
            {
                message("Ungültigen Gruppennamen (\"" + indexStr + "\") in Verzeichnis \"" +
                        localName + "\" gefunden", MsgType.Warning);
                return false;
            }

            return true;
        }

        enum MsgType
        {
            Info,
            Warning,
            Error,
            Debug
        }

        private static void message(string msg, MsgType type = MsgType.Info)
        {
            if (type == MsgType.Debug && !debug)
                return;

            var fg = Console.ForegroundColor;
            ConsoleColor newColor = ConsoleColor.White;

            switch (type)
            {
                case MsgType.Info:
                case MsgType.Debug:
                    newColor = ConsoleColor.White;
                    break;
                case MsgType.Warning:
                    newColor = ConsoleColor.DarkYellow;
                    break;
                case MsgType.Error:
                    newColor = ConsoleColor.Red;
                    break;
            }

            Console.ForegroundColor = newColor;
            Console.WriteLine(msg);
            
            Console.ForegroundColor = fg;
        }
    }
}