using System.Diagnostics;

namespace Correcter
{
    public class Executor
    {
        private string path;
        public bool linux;
        public Executor(string path, bool linux)
        {
            this.linux = linux;
            this.path = path;
        }

        public void execute(string program, string command)
        {
            if (linux)
            {
                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = program;
                psi.UseShellExecute = false;
                psi.Arguments = command;
                psi.RedirectStandardOutput = true;
                Process p = Process.Start(psi);
                p.WaitForExit();
                p.Close();
            }
            else
            {
                // This could probably just use the linux code too?
                ProcessStartInfo startInfo = new ProcessStartInfo("cmd.exe")
                {
                    // The arguments require a bit of a hack: We want to execute command and then exit.
                    // The problem is that command includes quotes (because the path might contain spaces)
                    // If we have
                    //     /k Path/to the/program.exe 
                    // then Path/to will be run (with the/program.exe as parameter), and we get an error
                    // If we have
                    //     /k "Path/to the/program.exe"
                    // the cmd.exe parser will helpfully remove the quotes, so this behaves the same as without quotes
                    // To avoid this, we can add some other command (like echo)
                    // and perhaps disguise it as a helpful message:
                    //     /k echo Opening & "Path/to the/program.exe"
                    // This way, we avoid the issue of nested quotes in cmd, which doesn't appear to be supported.
                    WorkingDirectory = path,
                    Arguments = "/k echo Opening & " + program + " " + command + " & exit"
                };
                Process cmd = Process.Start(startInfo);
                cmd.WaitForExit();
            }
        }
    }
}