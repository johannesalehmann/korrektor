using System;

namespace Correcter
{
    public class InputHelper
    {
        public static int getInt(string query)
        {
            Console.WriteLine(query);
            string input = Console.ReadLine();
            int number;

            while (!int.TryParse(input, out number))
            {
                Console.WriteLine("Bitte eine Zahl eingeben.");
                input = Console.ReadLine();
            } 
            
            return number;
        }
    }
}