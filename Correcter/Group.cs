using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Correcter
{
    public class Group
    {
        public int number;
        public List<string> members;
        public string directory;
        public string file;
        public string correctedProject;
        public string correctedPdf;

        public List<decimal> points;
        public decimal totalPoints => points.Sum();

        public Group(int number)
        {
            this.number = number;
            this.members = new List<string>();
            this.directory = null;
            this.file = null;
            this.correctedPdf = null;
            this.correctedProject = null;
            this.points = new List<decimal>();
        }

        public string serialize()
        {
            StringBuilder serialized = new StringBuilder();
            serialized.Append("Index:");
            serialized.Append(number);
            serialized.Append("\nMembers:");
            serialized.Append(string.Join(",", members));
            if (directory != null)
            {
                serialized.Append("\nDirectory:");
                serialized.Append(directory);
            }
            if (file != null)
            {
                serialized.Append("\nFile:");
                serialized.Append(file);
            }
            if (correctedProject != null)
            {
                serialized.Append("\nCorrectedProject:");
                serialized.Append(correctedProject);
            }
            if (correctedPdf != null)
            {
                serialized.Append("\nCorrectedPdf:");
                serialized.Append(correctedPdf);
            }

            serialized.Append("\nPoints:");
            serialized.Append(string.Join(";", points));

            return serialized.ToString();
        }

        public static Group deserialize(string[] lines)
        {
            Group group = new Group(-1); // I know, passing -1 as parameter here is not super pretty
            foreach (var line in lines)
            {
                if (line.StartsWith("Index:"))
                    int.TryParse(line.Substring("Index:".Length), out group.number);
                
                if (line.StartsWith("Members:"))
                    group.members = line.Substring("Members:".Length).Split(',').ToList();
                
                if (line.StartsWith("Directory:"))
                    group.directory = line.Substring("Directory:".Length);
                
                if (line.StartsWith("File:"))
                    group.file = line.Substring("File:".Length);
                
                if (line.StartsWith("CorrectedProject:"))
                    group.correctedProject = line.Substring("CorrectedProject:".Length);
                
                if (line.StartsWith("CorrectedPdf:"))
                    group.correctedPdf = line.Substring("CorrectedPdf:".Length);
                
                if (line.StartsWith("Points:"))
                    group.points = line.Substring("Points:".Length).Split(';').ToList().Select(str =>
                    {
                        if (decimal.TryParse(str, out decimal points))
                            return points;
                        return 0;
                    }).ToList();
            }

            return group;
        }
    }
}